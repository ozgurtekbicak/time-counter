// Countdown Time Counter

var date = new Date();
var endDate = date.setMinutes(date.getMinutes() + 20); // Currently 20 minutes countdown. You can change it.

if (localStorage.BasketCache === undefined) {
    var obj = {
        basketKey: "BasketKey_here", //$("#HiddenInputID_forBasketKey").val(),
        basketDate: endDate
    };

    localStorage.BasketCache = JSON.stringify(obj);
}
else {
    obj = JSON.parse(localStorage.BasketCache);
    endDate = obj.basketDate;
}

var _second = 1000;
var _minute = _second * 60;
var _hour = _minute * 60;
var _day = _hour * 24;
var timer;

function counterShow() {
    var nowDate = new Date();
    var diffTime = endDate - nowDate;
    if (diffTime < 0) {
        clearInterval(timer);
        document.getElementById('Div_LocalCounter').innerHTML = "Time is over"; // You can show alerts, modal windows etc.

        return;
    }
    var days = Math.floor(diffTime / _day);
    var hours = Math.floor((diffTime % _day) / _hour);
    var minutes = Math.floor((diffTime % _hour) / _minute);
    var seconds = Math.floor((diffTime % _minute) / _second);

    document.getElementById('Div_LocalCounter').innerHTML = hours + 'hrs ';
    document.getElementById('Div_LocalCounter').innerHTML = minutes + 'min ';
    document.getElementById('Div_LocalCounter').innerHTML += seconds + 'sec';

}
timer = setInterval(counterShow, 1000); 